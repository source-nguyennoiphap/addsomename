import redis

def set_sino():
    _redis = redis.Redis(db=0)
    sino_file = open('sino.txt', 'r')
    list_sino = sino_file.readlines()

    for sino in list_sino:
        csino, vsino = sino.split('=')
        _redis.mset({csino: vsino})

set_sino()