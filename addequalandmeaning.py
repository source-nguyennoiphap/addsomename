import redis
import time

def convert_name(lines):
    sino_database = redis.Redis(db=0)
    sino_targets = list()

    for line in lines:
        sino_target = ""
        for word in line:
            if word in sino_database:
                sino_target += sino_database[word].decode('utf-8')
            else:
                sino_target += word
        sino_targets.append(sino_target)

    return sino_targets

if __name__ == "__main__":
    name_corpus = open('fakenames.txt', 'r')
    original_lines = name_corpus.readlines()
    smooth_lines = list()

    for original_line in original_lines:
        smooth_lines.append(original_line.strip('\n'))

    sino_lines = convert_name(smooth_lines)
    target_file = open('realnames.txt', 'w+')

    if len(sino_lines) == len(smooth_lines):
        for i in range(len(sino_lines)):
            target_file.write(smooth_lines[i] + '=' + sino_lines[i].replace('\n', ' ').strip(' ') + "\n")
    else:
        raise Exception('Something wrong when convert')