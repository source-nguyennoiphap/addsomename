surnames_file = open('surnames.txt', 'r')
suffixs_file = open('suffixs.txt', 'r')

surnames = surnames_file.readlines()
suffixs = suffixs_file.readlines()

def smooth(list_word):
    result = list()

    for word in list_word:
        word = word.strip('\n')
        word = word.strip(' ')
        result.append(word)

    return result

surnames = smooth(surnames)
suffixs = smooth(suffixs)
targets = list()

for surname in surnames:
    for suffix in suffixs:
        chinese_suffix, viet_suffix = suffix.split('=')
        targets.append(surname + chinese_suffix)

output_file = open('fakenames.txt', 'w+')

for target in targets:
    output_file.write(target + '\n')

output_file.close()